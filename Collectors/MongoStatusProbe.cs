using System;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using Prometheus;
using unblock_it_dotnetcore.Tools;

namespace unblock_it_dotnetcore.Collectors
{
    public class MongoStatusProbe : IServiceMetricCollector
    {
        internal Counter _status { get; private set; } = default!;

        private readonly IMongoDatabase _database;

        private readonly ILogger _logger;

        public bool IsSupported => true;

        public MongoStatusProbe(IUnBlockITDatabaseSettings settings, ILogger<MongoStatusProbe> logger)
        {
            _database = new MongoClient(settings.ConnectionString)
                .GetDatabase(settings.DatabaseName);
            _logger = logger;
        }


        public void CreateMetrics(MetricFactory factory)
        {
            _status = factory.CreateCounter("datasource_connection_status",
                "datasource_connection_status", new CounterConfiguration
                {
                    LabelNames = new[] { "datasource" }
                });
            _status
                .WithLabels("mongo")
                .Inc(0);
        }

        public void UpdateMetrics()
        {
            var statusConnection = 0.0;

            try
            {
                statusConnection = _database
                    .RunCommand<BsonDocument>(new BsonDocument { { "ping", 1 } })
                    .GetElement("ok")
                    .Value
                    .AsDouble;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            _status
                .WithLabels("mongo")
                .IncTo(statusConnection);

        }
    }
}

