# UnblockIT

## What is monitoring?
O monitoramento é uma forma de se manter atualizado sobre a integridade e o desempenho de suas APIs. O serviço de monitoramento integrado do .Net Core ajuda a consolidar uma etapa adicional em seu ciclo de vida de desenvolvimento de API.

A identificação de erros de software e impacto nos negócios exige uma solução de monitoramento com a capacidade de observar e relatar como o sistema e os usuários se comportam. Os dados coletados devem fornecer as informações necessárias para analisar e identificar uma atualização incorreta. 

Respondendo a perguntas como:

* Estamos observando mais erros do que antes?
* Houve novos tipos de erros?
* A duração da solicitação aumentou inesperadamente em comparação com as versões anteriores?
* A taxa de transferência (req/s) diminuiu?
* O uso de CPU e/ou memória aumentou?
* Houve mudanças em nossos KPIs?
* outros pontos

O impacto de uma atualização incorreta do sistema pode ser minimizado combinando as informações de monitoramento com estratégias de implantação progressiva. Como canary, mirroring, rings, blue/green, etc.

## A observabilidade é construída em 3 pilares

Logging: coleta informações sobre eventos que acontecem no sistema. Ajudar a equipe a analisar o comportamento inesperado do aplicativo. 

Rastreamento: coleta informações para criar uma visão ponta a ponta de como as transações são executadas em um sistema distribuído.

Métricas: fornecem uma indicação em tempo real de como o sistema está funcionando. Ele pode ser aproveitado para criar alertas, permitindo ações reativas e pró-ativa a valores inesperados.

Na Obervalidade temos:
    * Log aggregation
    * Application metrics
    * Audit logging
    * Distributed tracing
    * Exception tracking
    * Health check API
    * Log deployments and changes

## Adicionando capacidade de observação a um aplicativo .NET Core

Existem muitas maneiras de adicionar aspectos de observabilidade a um aplicativo. Um exemplo é por meio do uso de malhas de serviço no Kubernetes (Istio, Linkerd), ou adicionando um middleware, etc.

O rastreamento integrado e transparente geralmente cobre cenários básicos e responde a perguntas genéricas, como duração da solicitação observada ou tendências de CPU. Outras questões, como KPIs personalizados ou comportamento do usuário, requerem a adição de instrumentação ao seu código.

O .NetCore através do [Microsoft.Extensions.Diagnostics.HealthChecks](https://docs.microsoft.com/pt-br/dotnet/api/microsoft.extensions.diagnostics.healthchecks.healthcheckservice?view=dotnet-plat-ext-3.1) oferece um middleware de verificações de integridade e bibliotecas para relatar a integridade dos componentes de infraestrutura de aplicativo.

As verificações de integridade são expostas por um aplicativo como pontos de extremidade HTTP. Os pontos de extremidade de verificação de integridade podem ser configurados para uma variedade de cenários de monitoramento em tempo real:

> Ex:

```shell
# AspNetCore.HealthChecks.AzureServiceBus
# AspNetCore.HealthChecks.AzureStorage
# AspNetCore.HealthChecks.DocumentDb
# AspNetCore.HealthChecks.SqlServer
# AspNetCore.HealthChecks.RabbitMQ
# AspNetCore.HealthChecks.MongoDb
# AspNetCore.HealthChecks.Npgsql
# AspNetCore.HealthChecks.MySql
# AspNetCore.HealthChecks.Redis
# AspNetCore.HealthChecks.Kafka
```

## Getting Started
Este exemplo ilustra o uso de `Health Check Middleware` e serviços.

Para executar o aplicativo de amostra para um cenário descrito no tópico, use o comando `dotnet run` da pasta do projeto em um shell de comando.

> Executando com `docker`

1. Realizar a build do projeto

```shell
dotnet restore -r linux-musl-x64
dotnet publish -c release -o ./build -r linux-musl-x64 --self-contained false --no-restore
```

2. Execute o comando para `build` do docker

```shell
docker build --cache-from unblock-it-dotnetcor:latest -t unblock-it-dotnetcor:latest .
```

3. Criar uma `network` com docker

```shell
docker network create unblock-it`
```

4. Execute o banco de dados `mongo`

```shell
docker run --rm \
    --name unblock-it-mongo \
    --network unblock-it \
    --publish 27017:27017 \
    --env MONGO_INITDB_DATABASE=unblock_it \
    --volume "$(pwd)"/Resources/DB/create.js:/docker-entrypoint-initdb.d/create.js \
    mongo
```

4. Rode o projeto com acesso ao `mongo`

```shell
docker run --name unblock-it-dotnetcore \
    --network unblock-it \
    --publish 80:80 \
    --env "UnBlockITDatabaseSettings__ConnectionString=mongodb://unblock-it-mongo:27017?connectTimeoutMS=2000" \
    --env "UnBlockITDatabaseSettings__DatabaseName=unblock_it" \
    unblock-it-dotnetcor:latest
```

5. Verificando Saúde da aplicação

> Default `curl -L http://localhost/health`

> Liveness `curl -L http://localhost/health/live`

> Readiness `curl -L http://localhost/health/ready`

6. Verificando Métricas 

```shell
curl -L http://localhost/metrics`
```

### Reference Documentation
Para referência futura, considere as seguintes seções:

* [Health Information](https://docs.microsoft.com/pt-br/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-3.1)
* [Prometheus](https://github.com/prometheus-net/prometheus-net)
* [Grafana](https://github.com/Cingulara/dotnet-core-prometheus-grafana)
* [Blog Observability](https://devblogs.microsoft.com/aspnet/observability-asp-net-core-apps/)
