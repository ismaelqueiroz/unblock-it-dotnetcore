using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Prometheus;
using unblock_it_dotnetcore.Collectors;

namespace unblock_it_dotnetcore.Tasks
{
    public class ServiceMetricsHostedService : IHostedService
    {
        private readonly IEnumerable<IServiceMetricCollector> _collectors;

        public ServiceMetricsHostedService(IEnumerable<IServiceMetricCollector> collectors)
        {
            _collectors = collectors;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            var registry = Metrics.DefaultRegistry;
            var factory = Metrics.WithCustomRegistry(registry);
   
            foreach (var collector in _collectors.Where(collector => collector.IsSupported))
            {
                collector.CreateMetrics(factory);
                registry.AddBeforeCollectCallback(collector.UpdateMetrics);
            }

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}